FROM debian:buster

WORKDIR /
RUN echo "deb-src http://deb.debian.org/debian testing main contrib non-free" \
  >> /etc/apt/sources.list && \
  apt-get update && \
  apt-get install -y packaging-dev debian-keyring devscripts equivs \
    build-essential vim htop mlocate sudo && \
  apt-get clean && \
  mkdir /srv/build && \
  useradd -d /srv/build -u 1000 -s /bin/bash -G sudo,users build && \
  chown build:build /srv/build -R && \
  sed -i "s/(ALL:ALL)/NOPASSWD :/g" /etc/sudoers
USER build
WORKDIR /srv/build/
CMD ["/bin/bash"]
